package com.nexos.infopersonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoPersonalApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoPersonalApplication.class, args);
	}

}
