package com.nexos.infopersonal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.infopersonal.model.TipoDocumento;
import com.nexos.infopersonal.service.TipoDocumentoService;

@RestController
@RequestMapping("/InfoPersonal/TipoDocumento")
public class TipoDocumentoController {
	
	@Autowired
	private TipoDocumentoService objTipoDocService;
	
	@GetMapping("/listarTipoDocumento")
	public List<TipoDocumento> listarTipoDocumento() {
		return objTipoDocService.listarTipoDocumento();
	}
	
	@PostMapping("/agregarTipoDocumento")
	public String agregarTipoDocumento(@RequestBody TipoDocumento objTipoDocumento) {
		String mensaje = "";
		mensaje = objTipoDocService.agregarTipoDocumento(objTipoDocumento);
		return mensaje;
	}
	
	@DeleteMapping(value = "/eliminarTipoDocumento/{idTipoDocumento}")
	public String eliminarTipoDocumento(@PathVariable("idTipoDocumento") int idTipoDocumento) {
		String mensaje = "";
		mensaje = objTipoDocService.eliminarTipoDocumento(idTipoDocumento);
		return mensaje;
	}

}
