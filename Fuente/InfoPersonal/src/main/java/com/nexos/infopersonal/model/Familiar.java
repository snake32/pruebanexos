/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.infopersonal.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "familiar")
public class Familiar implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "familiar_id")
    private Integer familiarId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombres_fam")
    private String nombresFam;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellidos_fam")
    private String apellidosFam;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono_fam")
    private String telefonoFam;
    
    @Size(max = 100)
    @Column(name = "direccion_fam")
    private String direccionFam;
    
    @JoinColumn(name = "parentesco_id", referencedColumnName = "parentesco_id")
    @ManyToOne(optional = false)
    private Parentesco parentescoId;
    
    @JoinColumn(name = "num_doc_per", referencedColumnName = "num_doc_per")
    @ManyToOne(optional = false)
    private Persona numDocPer;

    public Familiar() {
    }

    public Familiar(Integer familiarId) {
        this.familiarId = familiarId;
    }

    public Familiar(Integer familiarId, String nombresFam, String apellidosFam, String telefonoFam) {
        this.familiarId = familiarId;
        this.nombresFam = nombresFam;
        this.apellidosFam = apellidosFam;
        this.telefonoFam = telefonoFam;
    }

    public Integer getFamiliarId() {
        return familiarId;
    }

    public void setFamiliarId(Integer familiarId) {
        this.familiarId = familiarId;
    }

    public String getNombresFam() {
        return nombresFam;
    }

    public void setNombresFam(String nombresFam) {
        this.nombresFam = nombresFam;
    }

    public String getApellidosFam() {
        return apellidosFam;
    }

    public void setApellidosFam(String apellidosFam) {
        this.apellidosFam = apellidosFam;
    }

    public String getTelefonoFam() {
        return telefonoFam;
    }

    public void setTelefonoFam(String telefonoFam) {
        this.telefonoFam = telefonoFam;
    }

    public String getDireccionFam() {
        return direccionFam;
    }

    public void setDireccionFam(String direccionFam) {
        this.direccionFam = direccionFam;
    }

    public Parentesco getParentescoId() {
        return parentescoId;
    }

    public void setParentescoId(Parentesco parentescoId) {
        this.parentescoId = parentescoId;
    }

    public Persona getNumDocPer() {
        return numDocPer;
    }

    public void setNumDocPer(Persona numDocPer) {
        this.numDocPer = numDocPer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (familiarId != null ? familiarId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Familiar)) {
            return false;
        }
        Familiar other = (Familiar) object;
        if ((this.familiarId == null && other.familiarId != null) || (this.familiarId != null && !this.familiarId.equals(other.familiarId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nexos.infopersonal.model.Familiar[ familiarId=" + familiarId + " ]";
    }
    
}
