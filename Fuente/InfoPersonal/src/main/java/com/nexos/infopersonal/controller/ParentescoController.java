package com.nexos.infopersonal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.infopersonal.model.Parentesco;
import com.nexos.infopersonal.service.ParentescoService;

@RestController
@RequestMapping("/InfoPersonal/Parentesco")
public class ParentescoController {
	
	@Autowired
	private ParentescoService objParentescoService;
	
	@GetMapping("/listarParentesco")
	public List<Parentesco> listarParentesco() {
		return objParentescoService.listarParentesco();
	}
	
	@PostMapping("/agregarParentesco")
	public String agregarParentesco(@RequestBody Parentesco objParentesco) {
		String mensaje = "";
		mensaje = objParentescoService.agregarParentesco(objParentesco);
		return mensaje;
	}
	
	@DeleteMapping(value = "/eliminarParentesco/{idParentesco}")
	public String eliminarParentesco(@PathVariable("idParentesco") int idParentesco) {
		String mensaje = "";
		mensaje = objParentescoService.eliminarParentesco(idParentesco);
		return mensaje;
	}

}
