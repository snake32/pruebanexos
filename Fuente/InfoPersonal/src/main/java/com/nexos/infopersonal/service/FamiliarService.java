package com.nexos.infopersonal.service;

import java.util.List;
import java.util.Optional;

import com.nexos.infopersonal.model.Familiar;

public interface FamiliarService {
	
	public List<Familiar> listarFamiliar();
	public Optional<Familiar> listarFamiliarPorCodigo(int familiarId);
	public String agregarFamiliar(Familiar objFamiliar);
	public String eliminarFamiliar(int familiarId);

}
