package com.nexos.infopersonal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.infopersonal.model.Persona;
import com.nexos.infopersonal.repository.PersonaRepository;
import com.nexos.infopersonal.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {
	
	@Autowired
	private PersonaRepository objPersonaRepo;

	@Override
	public List<Persona> listarPersona() {
		return objPersonaRepo.findAll();
	}

	@Override
	public Optional<Persona> listarPersonaPorCodigo(int personaId) {
		return objPersonaRepo.findById(personaId);
	}

	@Override
	public String agregarPersona(Persona objPersona) {
		String estado = "";
		Persona objPer = objPersonaRepo.save(objPersona);
		if(!objPer.equals(null)) {
			estado = "Persona guardada exitosamente";
		}
		return estado;
	}

	@Override
	public String eliminarPersona(int personaId) {
		String estado = "";
		objPersonaRepo.deleteById(personaId);
		estado = "Persona eliminada exitosamente";
		return estado;
	}

}
