package com.nexos.infopersonal.service;

import java.util.List;
import java.util.Optional;

import com.nexos.infopersonal.model.Persona;

public interface PersonaService {
	
	public List<Persona> listarPersona();
	public Optional<Persona> listarPersonaPorCodigo(int personaId);
	public String agregarPersona(Persona objPersona);
	public String eliminarPersona(int personaId);

}
