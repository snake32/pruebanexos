package com.nexos.infopersonal.service;

import java.util.List;
import java.util.Optional;

import com.nexos.infopersonal.model.Parentesco;

public interface ParentescoService {
	
	public List<Parentesco> listarParentesco();
	public Optional<Parentesco> listarParentescoPorCodigo(int parentescoId);
	public String agregarParentesco(Parentesco objParentesco);
	public String eliminarParentesco(int parentescoId);

}
