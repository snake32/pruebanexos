/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.infopersonal.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author David
 */
@Entity
@Table(name = "tipo_documento")
public class TipoDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tip_documento_id")
    private Integer tipDocumentoId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "codigo_tip_doc")
    private String codigoTipDoc;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "nombre_tip_doc")
    private String nombreTipDoc;
    
    @Size(max = 200)
    @Column(name = "descripcion_tip_doc")
    private String descripcionTipDoc;

    public TipoDocumento() {
    }

    public TipoDocumento(Integer tipDocumentoId) {
        this.tipDocumentoId = tipDocumentoId;
    }

    public TipoDocumento(Integer tipDocumentoId, String codigoTipDoc, String nombreTipDoc) {
        this.tipDocumentoId = tipDocumentoId;
        this.codigoTipDoc = codigoTipDoc;
        this.nombreTipDoc = nombreTipDoc;
    }

    public Integer getTipDocumentoId() {
        return tipDocumentoId;
    }

    public void setTipDocumentoId(Integer tipDocumentoId) {
        this.tipDocumentoId = tipDocumentoId;
    }

    public String getCodigoTipDoc() {
        return codigoTipDoc;
    }

    public void setCodigoTipDoc(String codigoTipDoc) {
        this.codigoTipDoc = codigoTipDoc;
    }

    public String getNombreTipDoc() {
        return nombreTipDoc;
    }

    public void setNombreTipDoc(String nombreTipDoc) {
        this.nombreTipDoc = nombreTipDoc;
    }

    public String getDescripcionTipDoc() {
        return descripcionTipDoc;
    }

    public void setDescripcionTipDoc(String descripcionTipDoc) {
        this.descripcionTipDoc = descripcionTipDoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipDocumentoId != null ? tipDocumentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumento)) {
            return false;
        }
        TipoDocumento other = (TipoDocumento) object;
        if ((this.tipDocumentoId == null && other.tipDocumentoId != null) || (this.tipDocumentoId != null && !this.tipDocumentoId.equals(other.tipDocumentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nexos.infopersonal.model.TipoDocumento[ tipDocumentoId=" + tipDocumentoId + " ]";
    }
    
}
