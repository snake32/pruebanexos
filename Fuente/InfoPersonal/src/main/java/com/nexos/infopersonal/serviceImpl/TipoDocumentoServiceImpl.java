package com.nexos.infopersonal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.infopersonal.model.TipoDocumento;
import com.nexos.infopersonal.repository.TipoDocumentoRepository;
import com.nexos.infopersonal.service.TipoDocumentoService;

@Service
public class TipoDocumentoServiceImpl implements TipoDocumentoService {
	
	@Autowired
	private TipoDocumentoRepository objTipDocRepo;

	@Override
	public List<TipoDocumento> listarTipoDocumento() {
		return objTipDocRepo.findAll();
	}

	@Override
	public Optional<TipoDocumento> listarDocumentoPorCodigo(int tipoDocumentoId) {
		return objTipDocRepo.findById(tipoDocumentoId);
	}

	@Override
	public String agregarTipoDocumento(TipoDocumento objTipoDocum) {
		String estado = "";
		TipoDocumento objTipDoc = objTipDocRepo.save(objTipoDocum);
		if(!objTipDoc.equals(null)) {
			estado = "Tipo Documento guardado exitosamente";
		}
		return estado;
	}

	@Override
	public String eliminarTipoDocumento(int tipoDocumentoId) {
		String estado = "";
		objTipDocRepo.deleteById(tipoDocumentoId);
		estado = "Tipo Documento eliminado exitosamente";
		return estado;
	}

}
