package com.nexos.infopersonal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.infopersonal.model.Familiar;
import com.nexos.infopersonal.service.FamiliarService;

@RestController
@RequestMapping("/InfoPersonal/Familiar")
public class FamiliarController {
	
	@Autowired
	private FamiliarService objFamiliarService;
	
	@GetMapping("/listarFamiliar")
	public List<Familiar> listarFamiliar() {
		return objFamiliarService.listarFamiliar();
	}
	
	@PostMapping("/agregarFamiliar")
	public String agregarFamiliar(@RequestBody Familiar objFamiliar) {
		String mensaje = "";
		mensaje = objFamiliarService.agregarFamiliar(objFamiliar);
		return mensaje;
	}
	
	@DeleteMapping(value = "/eliminarFamiliar/{idFamiliar}")
	public String eliminarPersona(@PathVariable("idFamiliar") int idFamiliar) {
		String mensaje = "";
		mensaje = objFamiliarService.eliminarFamiliar(idFamiliar);
		return mensaje;
	}

}
