package com.nexos.infopersonal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.infopersonal.model.TipoDocumento;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Integer> {

}
