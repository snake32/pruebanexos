package com.nexos.infopersonal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.infopersonal.model.Familiar;
import com.nexos.infopersonal.repository.FamiliarRepository;
import com.nexos.infopersonal.service.FamiliarService;

@Service
public class FamiliarServiceImpl implements FamiliarService {
	
	@Autowired
	private FamiliarRepository objFamiliarRepo;

	@Override
	public List<Familiar> listarFamiliar() {
		return objFamiliarRepo.findAll();
	}

	@Override
	public Optional<Familiar> listarFamiliarPorCodigo(int familiarId) {
		return objFamiliarRepo.findById(familiarId);
	}

	@Override
	public String agregarFamiliar(Familiar objFamiliar) {
		String estado = "";
		Familiar objFam = objFamiliarRepo.save(objFamiliar);
		if(!objFam.equals(null)) {
			estado = "Familiar guardado exitosamente";
		}
		return estado;
	}

	@Override
	public String eliminarFamiliar(int familiarId) {
		String estado = "";
		objFamiliarRepo.deleteById(familiarId);
		estado = "Familiar eliminado exitosamente";
		return estado;
	}

}
