package com.nexos.infopersonal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.infopersonal.model.Genero;

public interface GeneroRepository extends JpaRepository<Genero, Integer> {

}
