package com.nexos.infopersonal.service;

import java.util.List;
import java.util.Optional;

import com.nexos.infopersonal.model.Genero;

public interface GeneroService {
	
	public List<Genero> listarGenero();
	public Optional<Genero> listarGeneroPorCodigo(int idGenero);
	public String agregarGenero(Genero objGenero);
	public String eliminarGenero(int idGenero);

}
