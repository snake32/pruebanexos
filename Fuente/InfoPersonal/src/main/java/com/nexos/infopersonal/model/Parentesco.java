/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.infopersonal.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author David
 */
@Entity
@Table(name = "parentesco")
public class Parentesco implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "parentesco_id")
    private Integer parentescoId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "codigo_paren")
    private String codigoParen;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_paren")
    private String nombreParen;
    
    @Size(max = 200)
    @Column(name = "descripcion_paren")
    private String descripcionParen;

    public Parentesco() {
    }

    public Parentesco(Integer parentescoId) {
        this.parentescoId = parentescoId;
    }

    public Parentesco(Integer parentescoId, String codigoParen, String nombreParen) {
        this.parentescoId = parentescoId;
        this.codigoParen = codigoParen;
        this.nombreParen = nombreParen;
    }

    public Integer getParentescoId() {
        return parentescoId;
    }

    public void setParentescoId(Integer parentescoId) {
        this.parentescoId = parentescoId;
    }

    public String getCodigoParen() {
        return codigoParen;
    }

    public void setCodigoParen(String codigoParen) {
        this.codigoParen = codigoParen;
    }

    public String getNombreParen() {
        return nombreParen;
    }

    public void setNombreParen(String nombreParen) {
        this.nombreParen = nombreParen;
    }

    public String getDescripcionParen() {
        return descripcionParen;
    }

    public void setDescripcionParen(String descripcionParen) {
        this.descripcionParen = descripcionParen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parentescoId != null ? parentescoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parentesco)) {
            return false;
        }
        Parentesco other = (Parentesco) object;
        if ((this.parentescoId == null && other.parentescoId != null) || (this.parentescoId != null && !this.parentescoId.equals(other.parentescoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nexos.infopersonal.model.Parentesco[ parentescoId=" + parentescoId + " ]";
    }
    
}
