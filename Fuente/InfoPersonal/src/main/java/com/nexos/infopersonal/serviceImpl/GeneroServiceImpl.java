package com.nexos.infopersonal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.infopersonal.model.Genero;
import com.nexos.infopersonal.repository.GeneroRepository;
import com.nexos.infopersonal.service.GeneroService;

@Service
public class GeneroServiceImpl implements GeneroService {
	
	@Autowired
	private GeneroRepository objGeneroRepo;

	@Override
	public List<Genero> listarGenero() {
		return objGeneroRepo.findAll();
	}

	@Override
	public Optional<Genero> listarGeneroPorCodigo(int idGenero) {
		return objGeneroRepo.findById(idGenero);
	}

	@Override
	public String agregarGenero(Genero objGenero) {
		String estado = "";
		Genero objGen = objGeneroRepo.save(objGenero);
		if(!objGen.equals(null)) {
			estado = "Genero guardado exitosamente";
		}
		return estado;
	}

	@Override
	public String eliminarGenero(int idGenero) {
		String estado = "";
		objGeneroRepo.deleteById(idGenero);
		estado = "Genero eliminado exitosamente";
		return estado;
	}

}
