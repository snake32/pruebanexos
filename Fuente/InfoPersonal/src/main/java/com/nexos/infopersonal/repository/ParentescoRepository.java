package com.nexos.infopersonal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.infopersonal.model.Parentesco;

public interface ParentescoRepository extends JpaRepository<Parentesco, Integer> {

}
