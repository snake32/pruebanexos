package com.nexos.infopersonal.service;

import java.util.List;
import java.util.Optional;

import com.nexos.infopersonal.model.TipoDocumento;

public interface TipoDocumentoService {
	
	public List<TipoDocumento> listarTipoDocumento();
	public Optional<TipoDocumento> listarDocumentoPorCodigo(int tipoDocumentoId);
	public String agregarTipoDocumento(TipoDocumento objTipoDocum);
	public String eliminarTipoDocumento(int tipoDocumentoId);

}
