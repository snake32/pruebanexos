package com.nexos.infopersonal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.infopersonal.model.Familiar;

public interface FamiliarRepository extends JpaRepository<Familiar, Integer> {

}
