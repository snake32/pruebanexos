/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.infopersonal.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "num_doc_per")
    private Long numDocPer;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombres_per")
    private String nombresPer;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellidos_per")
    private String apellidosPer;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono_per")
    private String telefonoPer;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "direccion_per")
    private String direccionPer;
    
    @JoinColumn(name = "genero_id", referencedColumnName = "genero_id")
    @ManyToOne(optional = false)
    private Genero generoId;
    
    @JoinColumn(name = "tip_documento_id", referencedColumnName = "tip_documento_id")
    @ManyToOne(optional = false)
    private TipoDocumento tipDocumentoId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numDocPer")
    private List<Familiar> familiarList;

    public Persona() {
    }

    public Persona(Long numDocPer) {
        this.numDocPer = numDocPer;
    }

    public Persona(Long numDocPer, String nombresPer, String apellidosPer, String telefonoPer, String direccionPer) {
        this.numDocPer = numDocPer;
        this.nombresPer = nombresPer;
        this.apellidosPer = apellidosPer;
        this.telefonoPer = telefonoPer;
        this.direccionPer = direccionPer;
    }

    public Long getNumDocPer() {
        return numDocPer;
    }

    public void setNumDocPer(Long numDocPer) {
        this.numDocPer = numDocPer;
    }

    public String getNombresPer() {
        return nombresPer;
    }

    public void setNombresPer(String nombresPer) {
        this.nombresPer = nombresPer;
    }

    public String getApellidosPer() {
        return apellidosPer;
    }

    public void setApellidosPer(String apellidosPer) {
        this.apellidosPer = apellidosPer;
    }

    public String getTelefonoPer() {
        return telefonoPer;
    }

    public void setTelefonoPer(String telefonoPer) {
        this.telefonoPer = telefonoPer;
    }

    public String getDireccionPer() {
        return direccionPer;
    }

    public void setDireccionPer(String direccionPer) {
        this.direccionPer = direccionPer;
    }

    public Genero getGeneroId() {
        return generoId;
    }

    public void setGeneroId(Genero generoId) {
        this.generoId = generoId;
    }

    public TipoDocumento getTipDocumentoId() {
        return tipDocumentoId;
    }

    public void setTipDocumentoId(TipoDocumento tipDocumentoId) {
        this.tipDocumentoId = tipDocumentoId;
    }

    @XmlTransient
    public List<Familiar> getFamiliarList() {
        return familiarList;
    }

    public void setFamiliarList(List<Familiar> familiarList) {
        this.familiarList = familiarList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numDocPer != null ? numDocPer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.numDocPer == null && other.numDocPer != null) || (this.numDocPer != null && !this.numDocPer.equals(other.numDocPer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nexos.infopersonal.model.Persona[ numDocPer=" + numDocPer + " ]";
    }
    
}
