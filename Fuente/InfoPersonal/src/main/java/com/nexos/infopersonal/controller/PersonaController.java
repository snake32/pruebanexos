package com.nexos.infopersonal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.infopersonal.model.Persona;
import com.nexos.infopersonal.service.PersonaService;

@RestController
@RequestMapping("/InfoPersonal/Persona")
public class PersonaController {
	
	@Autowired
	private PersonaService personaService;
	
	@GetMapping("/listarPersona")
	public List<Persona> listarPersona() {
		return personaService.listarPersona();
	}
	
	@PostMapping("/agregarPersona")
	public String agregarPersona(@RequestBody Persona objPersona) {
		String mensaje = "";
		mensaje = personaService.agregarPersona(objPersona);
		return mensaje;
	}
	
	@DeleteMapping(value = "/eliminarPersona/{idPersona}")
	public String eliminarPersona(@PathVariable("idPersona") int idPersona) {
		String mensaje = "";
		mensaje = personaService.eliminarPersona(idPersona);
		return mensaje;
	}

}
