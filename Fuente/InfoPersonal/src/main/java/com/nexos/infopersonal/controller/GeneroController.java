package com.nexos.infopersonal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.infopersonal.model.Genero;
import com.nexos.infopersonal.service.GeneroService;


@RestController
@RequestMapping("/InfoPersonal/Genero")
public class GeneroController {
	
	@Autowired
	private GeneroService objGeneroService;
	
	@GetMapping("/listarGenero")
	public List<Genero> listarGenero() {
		return objGeneroService.listarGenero();
	}
	
	@PostMapping("/agregarGenero")
	public String agregarGenero(@RequestBody Genero objGenero) {
		String mensaje = "";
		mensaje = objGeneroService.agregarGenero(objGenero);
		return mensaje;
	}
	
	@DeleteMapping(value = "/eliminarGenero/{idGenero}")
	public String eliminarGenero(@PathVariable("idGenero") int idGenero) {
		String mensaje = "";
		mensaje = objGeneroService.eliminarGenero(idGenero);
		return mensaje;
	}

}
