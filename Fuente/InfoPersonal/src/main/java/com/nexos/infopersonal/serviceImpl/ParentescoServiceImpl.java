package com.nexos.infopersonal.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.infopersonal.model.Parentesco;
import com.nexos.infopersonal.repository.ParentescoRepository;
import com.nexos.infopersonal.service.ParentescoService;

@Service
public class ParentescoServiceImpl implements ParentescoService {
	
	@Autowired
	private ParentescoRepository objParentescoRepo;

	@Override
	public List<Parentesco> listarParentesco() {
		return objParentescoRepo.findAll();
	}

	@Override
	public Optional<Parentesco> listarParentescoPorCodigo(int parentescoId) {
		return objParentescoRepo.findById(parentescoId);
	}

	@Override
	public String agregarParentesco(Parentesco objParentesco) {
		String estado = "";
		Parentesco objParent = objParentescoRepo.save(objParentesco);
		if(!objParent.equals(null)) {
			estado = "Parentesco guardado exitosamente";
		}
		return estado;
	}

	@Override
	public String eliminarParentesco(int parentescoId) {
		String estado = "";
		objParentescoRepo.deleteById(parentescoId);
		estado = "Parentesco eliminado exitosamente";
		return estado;
	}

}
