package com.nexos.infopersonal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.infopersonal.model.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {

}
